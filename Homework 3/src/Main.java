import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int inputNum = scanner.nextInt();
        int outputNum = 0;
        while (inputNum != 0){
            outputNum = outputNum + inputNum % 10;
            inputNum = inputNum / 10;
        }
        System.out.println(outputNum);
    }
}
