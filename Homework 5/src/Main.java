import java.util.Scanner;
import  java.util.Arrays;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int sizeofarray = scanner.nextInt();
        int[] array = new int[sizeofarray];
        for (int i = 0; i < array.length; i++){
            array[i] = scanner.nextInt();
        }
        System.out.println(Arrays.toString(array));

        for (int i = 0; i < sizeofarray /2 ; i++){
            int temp = array[i];
            array[i] = array[array.length -1 -i];
            array[array.length -1 -i] = temp;
        }
        System.out.println(Arrays.toString(array));
    }

}