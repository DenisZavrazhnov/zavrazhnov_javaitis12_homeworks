package ru.DZavrzhnov.web.controllers;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;



@Controller
public class MainController {




    @GetMapping("/")
    public String getUsersPage(Model model) {
        model.addAttribute("title", "TestApp");
        return "main_page";
    }
}
