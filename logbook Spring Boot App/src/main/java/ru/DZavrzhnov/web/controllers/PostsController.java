package ru.DZavrzhnov.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.DZavrzhnov.web.forms.PostForm;
import ru.DZavrzhnov.web.models.Post;
import ru.DZavrzhnov.web.service.PostService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@Controller
public class PostsController {

    @Autowired
    private PostService postService;



    @GetMapping("/posts")
    public String getPosts(@RequestParam(value = "type", required = false) String type, Model model){
        model.addAttribute("type", type);
        model.addAttribute("postForm", new PostForm());
        model.addAttribute("title","Отчет " + type);
        model.addAttribute("posts", postService.findAllByType(type));
        return "/posts_page";
    }

    @GetMapping("/search")
    public String getSearchPage(Model model){
        model.addAttribute("title","Ну что это такое?");
        return "/search_page";
    }

    @PostMapping("/addPost")
    public String addPost(@Valid PostForm postForm,
                          BindingResult bindingResult,
                          @RequestParam(value = "type", required = false) String type,
                          Model model) {
       if (bindingResult.hasErrors()){
           model.addAttribute("type",type);
           model.addAttribute("postForm", postForm);
           model.addAttribute("title","Отчет " + type);
           model.addAttribute("posts", postService.findAllByType(type));
           return "/posts_page";
       }
        postForm.setTypes(type);
        postService.addPost(postForm);
        if (type != null) {
            return "redirect:/posts?type=" + type;
        } else {
            return "redirect:/posts";
        }
    }


    @PostMapping("/posts/{id}/remove")
    public String deletePost(@RequestParam(value = "type", required = false) String type,
                             @PathVariable(value = "id") Long id, Model model){
        model.addAttribute("type", type);
        postService.findById(id).orElseThrow(IllegalArgumentException::new);
        postService.deleteById(id);
        if (type != null) {
            return "redirect:/posts?type=" + type;
        } else {
            return "redirect:/posts";
        }
    }

}
