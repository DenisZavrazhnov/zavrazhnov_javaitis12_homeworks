package ru.DZavrzhnov.web.models;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Table(name = "posts")
public class Post {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String numberOfLetters;
    private String organization;
    private String person;
    private String dateOfCreate;
    private String dateOfIssue;
    private String email;
    private Long authorId;
    private String types;

}
