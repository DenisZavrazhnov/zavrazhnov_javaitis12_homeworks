package ru.DZavrzhnov.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.DZavrzhnov.web.forms.PostForm;
import ru.DZavrzhnov.web.models.Post;
import ru.DZavrzhnov.web.repositories.PostRepository;

import java.util.List;
import java.util.Optional;

@Service
public class PostServiceImpl implements PostService {


    @Autowired
    private PostRepository postRepository;

    @Override
    public void addPost(PostForm postForm) {
        Post post =  Post.builder()
                .numberOfLetters(postForm.getNumberOfLetters())
                .organization(postForm.getOrganization())
                .person(postForm.getPerson())
                .email(postForm.getEmail())
                .dateOfCreate(postForm.getDateOfCreate())
                .dateOfIssue(postForm.getDateOfIssue())
                .types(postForm.getTypes())
                .build();
        postRepository.save(post);

    }

    @Override
    public List<Post> findAllByType(String type) {
        return postRepository.findAllByType(type);
    }

    @Override
    public Optional<Post> findById(Long id) {
        return postRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) {
        postRepository.deleteById(id);
    }

    @Override
    public List<Post> findAllByNumberOfLetters(String searchPattern) {
        return postRepository.findAllByNumberOfLetters(searchPattern);
    }
}
