package ru.DZavrzhnov.web.forms;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


@Data
public class PostForm {

    private String numberOfLetters;
    private String organization;
    @NotEmpty(message ="{errors.incorrect.person}" )
    @NotBlank(message ="{errors.incorrect.person}" )
    private String person;

    @NotNull(message = "{errors.incorrect.dateOfCreate}")
    private String dateOfCreate;
    private String dateOfIssue;
    @NotEmpty(message = "{errors.incorrect.email}")
    @Email(message = "{errors.incorrect.email}")
    private String email;
    private String types;
}
