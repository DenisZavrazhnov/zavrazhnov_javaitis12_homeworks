package ru.DZavrzhnov.web.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.DZavrzhnov.web.models.Post;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post,Long> {

    @Query("SELECT p FROM Post p WHERE (:type is null or p.types = :type)")
    List<Post> findAllByType(@Param("type") String type);

    List<Post> findAllByNumberOfLetters(String searchPattern);

}
