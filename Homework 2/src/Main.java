import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int input = scanner.nextInt();
        String stringin = Integer.toString(input);
        int size = stringin.length() - 1;
        int reversed = 0;
        int temp = 0;
        for (int i = size;input != 0; i--){
            temp = input % 10;
            reversed = reversed + (int)(temp * Math.pow(10,i));
            input = input /10;
        }
        System.out.println(reversed);
    }
}
